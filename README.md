# Entrepreneur Quotes

As I read articles and watch videos, I encounter the best quotes. Unfortunately, until today, I always forgot these awesome quotes eventually, or just don't find the source when I want to share a quote with someone.


## What is it going to be like, when it's ready?

I'd like to create a static website that creatively takes advantage of the great service of GitLab. The basic idea is that the page is hosted on [GitLab Pages](https://pages.gitlab.io/) and fetches a quote from the available pool of quotes, with all relevant information in JSON format. The frontend code in the browser will, then, parse this and display all relevant information in a manner that is a joy to look at.


## Guidelines for Me

* Use JavaScript for all supporting scripts and workflows. It's a simple project, with no other option than JavaScript on the client side, so I don't wish to add complexity to the project by having Python, Go, C, Bash scripts to perform simple tasks.
* Measure the size of the static page, minify, so the page is fast to load.
* Client-side and supporting code must be clean, organized, documented and tested (if I'll end up writing slightly more complex code than a `console.log("Hello");`)
* Try not to include every other `bower` package, but at the same time, I want to go for the widest reasonable browser support
* Write ES6 code

## Planned Features

* [ ] select a quote randomly from the pool of quotes. automatically let the client side script know the number of available quotes. Add test case to make sure that all enumerated quotes are available and validate that the JSON has the obligatory field (also, define obligatory fields).
* [ ] Write in a multilingual manner, as I have some Spanish quotes and videos that I like. There should be `es/x.json` and `en/a.json` endpoints and a switch button to query from these endpoints
* [ ] as we fetch a new quote, update `window.location.href` without reloading anything [(hint?)](http://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page)
* [ ] if the website is loaded with a quote in the URL, fetch that quote first
* [ ] pre-fetch a couple of quotes, providing a seamless user experience
* [ ] add images that are set as blurred background and change (?) either as the quote changes or as the time passes
* [ ] add testing framework and implement a couple of unit test cases
* [ ] add tests using selenium node http://webdriver.io/
* [ ] find or create docker image with modern Node, Selenium, Jasmine, etc...
* [ ] update build workflow
* [ ] add message to site and README: if I use a quote from you and you don't agree to it, open issue or send email
