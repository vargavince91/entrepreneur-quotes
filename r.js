console.log("zzz");

// Fix for iPhone: http://stackoverflow.com/questions/18914568/make-onclick-work-on-iphone
var click = "ontouchstart" in window ? "touchstart" : "click";

document.getElementById("example-quote").addEventListener(click, fetchExample);

function fetchExample () {
    // TODO: Add polyfills
    fetch('./json/q0001.json')
        .then(function (r) {
            if (r.status === 200) {
                r.json().then(function (d) {
                    document.getElementById("example-target").innerHTML = d.q.txt;
                });
            }
        });
}
